<?php
	// if(empty($viewData))
		// redirect(base_url(),"refresh");
?>
<ul class="breadcrumb">
	<!--<li><a href="index.html">Home</a> <span class="divider">/</span></li>
	<li class="active">Products Name</li>-->
	<?php 
		$breadCrumb = $obj->getBreadCrumb();
		foreach($breadCrumb as $breadcrumbs)
		{
			echo '<li>
					<a href="'.$breadcrumbs["link"].'">'.$breadcrumbs["name"].'</a><span class="divider">/</span>
				</li>';
		}
	?>
</ul>	
	<table class="table table-bordered">
    <thead>
    	<tr>
        	<th>Product</th>
            <th>Product Name</th>
			<th>Rate</th>
            <th>Discount Price</th>
            <th>Quantity/Update</th>
            <th>Total</th>
		</tr>
    </thead>
    <tbody>
	  	<?php
			foreach($cartData["productData"] as $products)
			{
				echo '<tr prod = "'.$products["row_id"].'" style = "position:relative;">
	                  <td> <img width="60" src="'.$products['product_image'].'" alt=""/></td>
	                  <td>'.$products['product_name'].'</td>
		   			  <td class = "prod_price">'.$products['product_price'].'</td>
				      <td  class = "disc_price">';
				echo ($products['discount_status'] == "1")? $products['discount_price'] : "&ndash;";					
				echo '</td>
				      <td>
						<center>
							<div class="input-append"><input class="span1 prod_qty" style="max-width:34px" placeholder="1" id="appendedInputButtons" size="16" type="text" value = "'.$products['quantity'].'">
							<button class="btn minusqty" type="button"><i class="icon-minus"></i></button>
							<button class="btn plusqty" type="button"><i class="icon-plus"></i></button>
							<button class="btn btn-info savechg show-tooltip" title = "Save Changes"  data-rel="tooltip" type="button"><i class="icon-ok"></i></button>
							<button class="btn btn-danger remove_prod show-tooltip" title = "Remove Product"  data-rel="tooltip" type="button"  link = "'.base_url().'checkout/removeItem/'.$products['row_id'].'"><i class="icon-remove icon-white"></i></button></div>				
						</center>
								  </td> ';
						
						echo	  '<td class = "prod_tot_price">'.$products['totalprice'].'</td>
				                </tr>';
					}
				?>
                <tr>
                  <td colspan="5" style="text-align:right">Total Price:	</td>
                  <td class = "tot_price"> <?php echo $cartData["totalprice"]; ?></td>
                </tr>
				 <tr>
                  <td colspan="5" style="text-align:right">Total Discount:	</td>
                  <td class = "tot_disc_price"> <?php echo $cartData["totaldiscount"]; ?></td>
                </tr>
				 
				</tbody>
            </table>