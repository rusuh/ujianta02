<?php include("securearea.php"); ?>
<?php include("secureaccess.php"); ?>
<?php
class Customerinvoice extends Securearea
{
	// public $posteddata = "";
	// public $invoices = "";
	// public $products = array();
	// public $vendors = array();
	// public $users = array();

	function __construct()
	{
		parent::__construct();
		// $this->load->helper("url");
		$this->load->library("customtable_lib");		
		// $this->load->model("admin/purchase_invoice_model");		
		// $this->load->model("admin/product_model");	
		// $this->load->model("admin/vendors_model");	
		// $this->load->model("admin/adminuser_model");
	}

	public function index()
	{
		$this->viewCustomerInvoice();
	}

	public function viewCustomerInvoice()
	{
		$this->load->model("admin/sales_model");
		$this->loadHeader($this,FALSE,"Customer Invoices");
		$this->loadSidebar($this);

		$view['oObj'] = $this;
		$cInvoiceData = $this->sales_model->getSalesInvoice();

		$headings = array
		( 
			"order_id"=>"Order No.",
			"customer_name"=>"Customer Name",
			
			"order_date"=>"Order Date",
			"shipping_address"=>"Shipping Address",
			"shipping_area"=>"Area",
			"shipping_pin"=>"Shipping PIN",
			"vendor_details"=>"Vendor Details",
			"creator_name"=> "Created By",
			"created_date"=>"Created On"
		);
		$label = "Customer Sales Invoice";
		$action = array
		(
			"btns"=>array("view"),
			"text"=>array("View Sales Invoice"),
			"dbcols"=>array("order_id"),
			"link"=>array(base_url()."admin/sales/disp_invoice/%@$%"),
			"clickable"=>array()
		);
		
		$tableData = $this->customtable_lib->formatTableCells($label,$headings,$cInvoiceData,"",$action);
		$tableData["descFirst"] = TRUE;

		$this->load->view("customerinv_view",$view);

		$this->loadFooter($this);
	}
}
?>